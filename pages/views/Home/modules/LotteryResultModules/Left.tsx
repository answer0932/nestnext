import React, { useEffect, useState, useCallback, useRef } from 'react';
import { useEffectOnce } from 'react-use';
import { Row, Col } from 'react-bootstrap';
import dynamic from 'next/dynamic';
import ProtoType from 'prop-types';
import Moment from 'moment';

const CountDown = dynamic(() => import(
    /* webpackChunkName: "countDown" */
    /* webpackMode: "lazy" */
    "components/countDown"), { ssr: false, });

type PropsType = {
    data: {
        countDown: number;
        date: string;
        price: number;
        slot: string;
        time: string;
        translate: string;
        week: string;
        ball: Array<string | number>;
        odd: string;
    }
}

type slotTypes = "AM" | "PM";

const Left = ({ data }: PropsType) => {

    const [status, setStatus] = useState(true);
    const [count, setCount] = useState(10);
    const [ball, setBall] = useState<Array<string | number>>(["？", "？", "？"]);
    const [date, setDate] = useState(Moment().format("DD/MM/YY"));
    const [odd, setOdd] = useState("Odd of Winning: ? in ?");
    const [price, setPrice] = useState('0.00');
    const [slot, setSlot] = useState<string>('AM' as slotTypes);
    const [time, setTime] = useState(Moment().format("hh:mm"));
    const [translate, setTranslate] = useState("€");
    const [week, setWeek] = useState(Moment.weekdaysShort('-MMM-', Moment().day()));

    const timer: any = useRef();

    /**
     * @func countDownHandle
     * @desc if count = 0 just use fetch get new Data , get new data after must use countDown go to setCount; 
     */
    const countDownHandle = async () => {
        const response = await fetch("/lottery");
        const { data } = await response.json();

        if (data) {
            const { countDown, date, price, slot, time, translate, week } = data;
            setCount(countDown);
            setBall(ball);
            setDate(date);
            setOdd(odd);
            setPrice(price.toFixed(2));
            setSlot(slot);
            setTime(time);
            setTranslate(translate);
            setWeek(week);
        }
    };

    /**
     * @func installDataHandle 
     * @desc only once use this function handle
     */
    const installDataHandle = useCallback(() => {
        if (!status) {
            setCount(10000);
        } else {
            const { countDown, date, price, slot, time, translate, week } = data;
            setCount(countDown);
            setBall(ball);
            setDate(date);
            setOdd(odd);
            setPrice(price.toFixed(2));
            setSlot(slot);
            setTime(time);
            setTranslate(translate);
            setWeek(week);
        }
    }, [status, data]);

    /**
     * @func startHandle
     * @desc setTimeout countdown function, get new date of countDown field put setCount
     *       if > 0 Diminishing else use countDownHandle get new data and Destroy once setTimeout
     */
    const startHandle = useCallback(() => {
        timer.current = setInterval(() => {
            if (count > 0) {
                setCount(count - 1);
            } else {
                countDownHandle();
                clearInterval(timer.current);
            }
        }, 1000);
    }, [count]);


    useEffectOnce(() => {
        console.log('render once Effect to once');

        if (data) {
            setStatus(true);
        } else {
            setStatus(false);
        }

        installDataHandle();
    })


    useEffect(() => {

        console.log('start');

        startHandle();

        return () => {
            clearInterval(timer.current);
        };
    }, [count, startHandle])



    return (
        <Col xs={12} lg={5} className="bg-danger p-3 text-white">
            <Row>
                <Col>Next Drawing:</Col>
                <Col>Top Prize:</Col>
            </Row>
            <Row className="mt-3 fs-5">
                <Col>{week}</Col>
                <Col>{translate} {price}</Col>
            </Row>
            <Row className="fs-5">
                <Col>{date}</Col>
            </Row>
            <Row className="fs-5 pb-5" >
                <Col>{time} {slot}</Col>
            </Row>
            <CountDown count={count} />
        </Col>)
}

Left.prototype = {
    data: ProtoType.objectOf(ProtoType.shape({
        countDown: ProtoType.number,
        date: ProtoType.string,
        price: ProtoType.number,
        slot: ProtoType.string,
        time: ProtoType.string,
        translate: ProtoType.string,
        week: ProtoType.string
    }))
}

export default Left;