import React from 'react';
import { Row, Col } from 'react-bootstrap';
import ProtoType from 'prop-types';

type CountDownTypes = {
    count: number
};

const CountDownView = ({ count }: CountDownTypes) => {

    return (
        <Row className="text-white">
            <Row className="fs-6 pb-4 text-center">
                <Col>Live Drawing in:</Col>
            </Row>
            <Col className="px-0 text-center">
                <p className="fs-1">{Math.floor((count / 3600) % 24) <= 10 ? '0' : ''}{Math.floor((count / 3600) % 24)}</p>
                <p>HOURS</p>
            </Col>
            <Col className="px-0 text-center">
                <p className="fs-1">{Math.floor((count / 60) % 60) < 10 ? '0' : ''}{Math.floor((count / 60) % 60)}</p>
                <p>MINS</p>
            </Col>
            <Col className="px-0 text-center">
                <p className="fs-1">{Math.floor(count % 60) < 10 ? '0' : ''}{Math.floor(count % 60)}</p>
                <p>SECS</p>
            </Col>
        </Row>
    )
}

CountDownView.prototype = {
    count: ProtoType.number
}

export default CountDownView;