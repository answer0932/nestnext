import React from 'react';
import { useRecoilState } from "recoil";
import { Button, Modal } from 'react-bootstrap';
import ProtoType from 'prop-types';
import { modalState } from 'recoil/modal.recoil';
import Video from 'components/video';

type PropsType = {
    title: string;
}

const ModalView = ({ title }: PropsType) => {

    const [show, setShow] = useRecoilState(modalState);

    const handleClose = () => setShow(false);

    return (
        <>
            <Modal show={show} onHide={handleClose} backdrop="static" size="lg">
                <Modal.Header closeButton>
                    <Modal.Title>{title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Video />
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

ModalView.prototype = {
    title: ProtoType.string
}

export default ModalView;