/** @type {import('next').NextConfig} */
// module.exports = {
//     reactStrictMode: true,
//     images: {
//         domains: [],
//     },
//     optimization: {
//         splitChunks:{

//         }
//     }
// }
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
const TerserPlugin = require("terser-webpack-plugin");
const ImageMinimizerPlugin = require("image-minimizer-webpack-plugin");
const { extendDefaultPlugins } = require("svgo");

module.exports = {
    webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
        console.log(config, 'webpack');

        config.module.rules.push(
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                type: "asset",
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    {
                        loader: ImageMinimizerPlugin.loader,
                        options: {
                            severityError: "warning", // Ignore errors on corrupted images
                            minimizerOptions: {
                                plugins: ["gifsicle", "jpegtran", "optipng", "svgo"],
                            },
                        },
                    },
                ],
            }
        )
        config.plugins.push(
            new MomentLocalesPlugin({
                localesToKeep: ['es-us'],
            }),
            new ImageMinimizerPlugin(
                {

                    minimizerOptions: {
                        encodeOptions: {
                            mozjpeg: {
                                // That setting might be close to lossless, but it’s not guaranteed
                                // https://github.com/GoogleChromeLabs/squoosh/issues/85
                                quality: 100,
                            },
                            webp: {
                                lossless: 1,
                            },
                            avif: {
                                // https://github.com/GoogleChromeLabs/squoosh/blob/dev/codecs/avif/enc/README.md
                                cqLevel: 0,
                            },
                        },
                        // Lossless optimization with custom option
                        // Feel free to experiment with options for better result for you
                        plugins: [
                            ["gifsicle", { interlaced: true }],
                            ["jpegtran", { progressive: true }],
                            ["optipng", { optimizationLevel: 5 }],
                            // Svgo configuration here https://github.com/svg/svgo#configuration
                            [
                                "svgo",
                            ],
                        ],
                    },
                })
        )
        config.optimization.minimize = true;
        config.optimization.minimizer.push(
            new TerserPlugin({
                parallel: true, // 可省略，默认开启并行
                terserOptions: {
                    toplevel: true, // 最高级别，删除无用代码
                    ie8: true,
                    safari10: true,
                }
            })
        );

        config.externals.push('moment') // build 

        // Important: return the modified config
        return config
    },
}