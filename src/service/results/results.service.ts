import { Injectable } from '@nestjs/common';
import { LotteryTypes } from '@interfaces/home.d';
import { DateListTypes, DateSelectionTypes } from '@interfaces/results.d';
import { UtilsService } from '../../service/utils/utils.service';

type DataType = {
    selection: DateSelectionTypes;
    list: DateListTypes;
    lottery: LotteryTypes;
};

@Injectable()
export class ResultsService {
    async resultaData(): Promise<{ data: DataType | null; status: boolean; msg: string; }> {
        return new Promise((resolve) => {
            const data = new UtilsService().publicDataFormat({
                selection: [],
                list: [],
                lottery: {
                    price: Math.random() * 5,
                    translate: '€',
                    week: 'Mon',
                    date: '10/04/21',
                    time: '2:00',
                    slot: 'AM',
                    ball: [Math.round(Math.random() * 9), Math.round(Math.random() * 9), Math.round(Math.random() * 9)],
                    odd: 'Odd of Winning: 1 in 216',
                    countDown: 10
                },
                video: {
                    mp4: {
                        src: '//vjs.zencdn.net/v/oceans.mp4',
                        type: 'video/mp4',
                        poster: '//vjs.zencdn.net/v/oceans.png'
                    }
                },
            }, '', true);

            resolve(data);
        })
    }
}
