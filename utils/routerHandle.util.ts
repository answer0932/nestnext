import { useRouter } from 'next/router';

export const currentPath = () => {
    const { asPath } = useRouter();
    return asPath;
}