import React from 'react';
import Image from 'next/image';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';

const CheckMyNumbers = () => {
    return (
        <Container className="mt-3">

            <Row className="justify-content-between">
                <Col lg={8} md={12} className="p-4 bg-white">

                    <h3 className="text-danger">Check my numbers</h3>
                    <p>Are you holding on to a winning ticket? Here s an<br />
                        easy way to find out.
                    </p>

                    <Row>
                        <Col>
                            <h6> Enter your numbers</h6>

                            <Row>
                                <Col lg={3}><input type="text" className="form-control col-4" aria-describedby="passwordHelpInline" /></Col>
                                <Col lg={3}><input type="text" className="form-control col-4" aria-describedby="passwordHelpInline" /></Col>
                                <Col lg={3}><input type="text" className="form-control col-4" aria-describedby="passwordHelpInline" /></Col>
                            </Row>

                            <Row>
                                <Col lg={8}>
                                    <Form.Select aria-label='Default Date select example' className="mt-5" defaultValue="0">
                                        <option>Open this select Date</option>
                                        <option value="0">Today</option>
                                        <option value="2">Last 2 Days</option>
                                        <option value="5">Last 5 Days</option>
                                        <option value="8">Last 8 Days</option>
                                        <option value="30">Last 30 Days</option>
                                        <option value="180">Last 180 Days</option>
                                    </Form.Select>
                                </Col>
                            </Row>
                        </Col>
                        <Col>
                            <h6>Select Match Order</h6>
                            <Form.Select className="form-select" aria-label="Default Match select example" defaultValue="Exact">
                                <option value="Exact">Exact</option>
                                <option value="Any">Any</option>
                            </Form.Select>

                            <Button className="rounded-pill fs-6 mt-5">CHECK MY NUMBERS</Button>
                        </Col>
                    </Row>
                </Col>
                <Col lg={3} md={12} className="bg-primary p-4 generate-ground">

                    <div className="d-flex justify-content-center align-items-center pb-4">
                        <Image src="/images/home/random-number-generator-icon.png" width="194" height="108" alt={''} />
                    </div>

                    <p className="text-white">Generate Random <br />Numbers </p>

                    <p className="text-white">enerate random numbers with our<br />interactive tool. </p>

                    <Button className="rounded-pill btn-light text-primary fs-6 mt-4">Generate Numbers</Button>
                </Col>
            </Row>
        </Container>
    )
}

export default CheckMyNumbers;