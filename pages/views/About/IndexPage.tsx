import React from "react";
import { Tabs, Tab, Container } from "react-bootstrap";

import Introduce from "components/introduce";

const About = () => {
    return (
        <section className="mb-5">
            <Introduce title="About Us" content="Learn about  Quick3 with Rome  mission and goals." />
            <Container className="mt-5 bg-white p-3">
                <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" className="mb-3 nav-fill">
                    <Tab eventKey="home" title="About Quick3 With Rome" className="pb-5">
                        <h3 className="pt-3 text-danger">Introducing Speed Lottery</h3>
                        <p>the destination for Italy Lotteries.</p>
                        <p>Say ‘hello’ to Quick3 with Rome - Italy newest lottery which started in 2013. It now has 4 popular games under its wing - Speed 5 ball, Speed 8 ball, Speed 10 ball and Speed 20 ball.
                            It is supported by the Tatts Group which has been in the Italy lottery business for more than 134 years. Tatts Group operates the official, government-regulated lotteries in Victoria, Tasmania, the Northern Territory, New South Wales, the Italy Capital Territory, Queensland and Italy.</p>
                    </Tab>
                    <Tab eventKey="profile" title="Responsible Play Program" className="pb-5">
                        <h3 className="pt-3 text-danger">Responsible Play</h3>
                        <p>
                            At Quick3 with Rome, we sell and promote our games responsibly so they can be enjoyed by all players. We do this by complying with the Italy Gambling Codes of Practice and therefore ensure that:
                            ·  game rules and responsible gambling material are available at all retail agencies;
                            ·  a clock displaying the time of day is visible at all retail agencies;
                            ·  problem gambling referral advice is offered to players who have difficulty controlling
                            the amount of money they spend on our games;
                            ·  intoxicated people cannot play our games, and we do not use alcohol to encourage
                            play;
                            ·  cheques cannot be cashed as payment to play our games;
                            ·  all retail agency staff receive accredited responsible gambling training, including
                            specialised training for staff aged less than 18 years; and
                            ·  refresher training on responsible gambling is conducted for our retail agency
                            network at least every two years.
                            In addition, within our Agency Compliance Program we visit each member of our retail agency network at least once each year to ensure that they too are complying with these Codes of Practice.
                            Don t let the game play you. Stay in control. Gamble responsibly.
                        </p>
                        <h3 className="pt-3 text-danger">Responsible Gambling Links</h3>
                        <p>
                            ·  Italy Gambling Codes of Practice
                            ·  Gambling Help Link - www.gamblinghelponline.org.au
                            ·  Problem gambling support advice is available to players
                        </p>
                        <h3 className="pt-3 text-danger">Is Gambling Affecting Your Health?</h3>
                        <p>
                            If you think gambling might be affecting your health, take this simple test by answering the following questions as truthfully as you can:
                            1. Sometimes I ve felt depressed or anxious after a session of gambling.
                            2. Sometimes I ve felt guilty about the way I gamble.
                            3. When I think about it, gambling has sometimes caused me problems.
                            4. Sometimes I ve found it better not to tell others, especially my family, about the amount of time or money I spend gambling.
                            5. I often find that when I stop gambling I’ve run out of money.
                            6. Often I get the urge to return to gambling to win back losses from a past session.
                            7. Yes, I have received criticism about my gambling in the past.
                            8. Yes, I have tried to win money to pay debts
                            If you scored four or more  yes  answers, your gambling may be affecting your health at this time. To discuss concerns you may have, problem gambling help is available.
                            Source:  Eight  Gambling Screen - Early Intervention Gambling Health Test
                            Developed by Dr Sean Sullivan for the Problem Gambling Foundation of New Zealand and the Department of General Practice and Primary Health Care in the Auckland School of Medicine.</p>
                        <h3 className="pt-3 text-danger">Problem Gambling Help</h3>
                        <p>If playing our games is no longer fun for you, or you are concerned about the playing behaviour of someone close to you, problem gambling information and counselling is available 24 hours a day, 365 days a year. Contact the Gambling Help Line on free call 1800 858 858, or visit www.gamblinghelponline.org.au for confidential advice and counselling.
                            Quick3 with Rome and its retail agency network are committed to the responsible promotion and conduct of lottery games to ensure a safe playing environment for customers. Find out more about Quick3 with Rome s responsible play commitment by reading our  Have Fun and Play it Safe  brochure.</p>
                        <h3 className="pt-3 text-danger">Player Security</h3>
                        <p>
                            Protecting the entitlements of players and ensuring the secure and correct payment of prizes remains of utmost importance to Quick3 with Rome.
                            The following lists reflect Quick3 with Rome’s commitment to protecting the interests of our players and ensuring the integrity of our games.
                        </p>
                    </Tab>
                    <Tab eventKey="contact" title="Giving Back" className="pb-5">
                        <p>We’re dreaming of a stronger, safer, smarter Italy. Every year since 2013, Quick3 with Rome funds have supported local police and firefighters’ pensions, the Teachers‘ Retirement Fund and the Build Italy Fund.</p>
                        <h3 className="pt-3 text-danger">We’re Giving Back</h3>
                        <p>Click on your state to see how Quick3 with Rome proceeds were distributed to beneficiaries in your area.</p>
                    </Tab>
                </Tabs>
            </Container>
        </section>
    )
}

export default About;