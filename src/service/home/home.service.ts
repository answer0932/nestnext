import { Injectable } from '@nestjs/common';
import { VideoTypes, SwiperTypes, LotteryTypes } from '@interfaces/home';
import { UtilsService } from '../../service/utils/utils.service';

type DataType = {
    video: VideoTypes;
    swiper: SwiperTypes;
    lottery: LotteryTypes;
};

@Injectable()
export class HomeService {
    async homeData(): Promise<{ data: DataType | null; status: boolean; msg: string; }> {
        return new Promise((resolve) => {
            const data = new UtilsService().publicDataFormat({
                video: {
                    mp4: {
                        src: '//vjs.zencdn.net/v/oceans.mp4',
                        type: 'video/mp4',
                        poster: '//vjs.zencdn.net/v/oceans.png'
                    }
                },
                swiper: [{
                    id: Math.random(),
                    url: '/images/banner/0.jpg'
                },
                {
                    id: Math.random(),
                    url: '/images/banner/1.jpg'
                },
                {
                    id: Math.random(),
                    url: '/images/banner/2.jpg'
                }],
                lottery: {
                    price: Math.random() * 5,
                    translate: '€',
                    week: 'Mon',
                    date: '10/04/21',
                    time: '2:00',
                    slot: 'AM',
                    ball: [Math.round(Math.random() * 9), Math.round(Math.random() * 9), Math.round(Math.random() * 9)],
                    odd: 'Odd of Winning: 1 in 216',
                    countDown: 10
                }
            }, '', true);

            resolve(data);
        })
    }

    async lottery(): Promise<{ data: LotteryTypes | null; status: boolean; msg: string; }> {
        return new Promise(resolve => {
            const data = new UtilsService().publicDataFormat({
                price: Math.random() * 5,
                translate: '€',
                week: 'Mon',
                date: '10/04/21',
                time: '2:00',
                slot: 'AM',
                ball: [Math.round(Math.random() * 9), Math.round(Math.random() * 9), Math.round(Math.random() * 9)],
                odd: 'Odd of Winning: 1 in 216',
                countDown: 300
            }, '', true);

            resolve(data);
        })
    }
}
