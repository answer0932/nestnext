import React from 'react'

export default function GameRules() {
    return (
        <div className="mt-3">
            <div className="container-fluid container bg-white p-4">
                <h3 className="text-danger">How to Play Pick 3 With Rome</h3>
                <h6>Pick just 6 numbers from 1-47 and if your numbers match those drawn you can win a minimum of over $3
                    Million (€2
                    Million) from Irish Lotto! It s that simple!
                    <br />
                    <br />
                    <br />
                    Draws are held every Thursday and Sunday (AEST) and when you
                    purchase an entry in our Irish Lotto draw, we will purchase a matching ticket in the Lotto draw conducted in
                    Ireland. For more primary, check out a summary of the .
                </h6>
                <br />
                <a href="/rules" className="link-primary">Lottery Operator s rules</a>
                <h6 className="mt-1">Simplicity and reasonable odds make this a great lotto to
                    play and everyone knows the luck of the Irish can strike anywhere, anytime.</h6>
            </div>
        </div>
    )
}
