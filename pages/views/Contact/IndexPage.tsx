import React from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import Introduce from 'components/introduce';

export default function IndexPage() {
    return (
        <section className="mb-5">
            <Introduce title="Contact Us" content="For all enquiries, please contact us using the form below." />
            <Container className="mt-5">
                <div className="alert alert-danger" role="alert">
                    <h6> A word of warning </h6>
                    <p className="text-body">Be aware of scammers and false announcements about lottery prizes.</p>
                    <p className="text-body">
                        Remember, Lottery USA does not contact users to tell them they have won a lottery prize. Any message you receive informing you that you’ve won a prize for a lottery you did not enter is fraudulent. These messages can arrive via phone, email, text message, Facebook, or WhatsApp. Never respond to such messages, and never ever provide your money or sensitive information such as Social Security, bank account, or credit card numbers to anyone.
                    </p>
                </div>

                <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Your Name</Form.Label>
                        <Form.Control placeholder="Enter text here..." />
                        {/* <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                        </Form.Text> */}
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Your Email Address</Form.Label>
                        <Form.Control type="email" placeholder="Enter text here..." />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <Form.Label>Email Topic</Form.Label>
                        <Form.Select defaultValue="One">
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </Form.Select>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Message</Form.Label>
                        <Form.Control as="textarea" rows={5} />
                    </Form.Group>
                    <div className="d-grid gap-2">
                        <Button variant="primary" size="lg" className="rounded-pill">
                            SEND
                        </Button>
                    </div>
                </Form>
            </Container>
        </section>
    )
}
