import { Controller, Get, Render, Res } from '@nestjs/common';
import { HomeService } from '../../service/home/home.service';
import { UtilsService } from '../../service/utils/utils.service';


@Controller()
export class HomeController {
    constructor(private readonly homeService: HomeService, private readonly utilsService: UtilsService) { }

    @Get()
    @Render('Home/IndexPage')
    async index() {

        try {
            const data = await this.homeService.homeData();

            return this.utilsService.publicDataFormat(data.data, 'ok', true);

        } catch (error) {

            return this.utilsService.publicDataFormat(null, 'no', false);

        }

    }


    @Get('lottery')
    async home() {

        try {
            const { data } = await this.homeService.lottery();

            return this.utilsService.publicDataFormat(data, 'ok', true);

        } catch (error) {

            return this.utilsService.publicDataFormat(null, 'no', false);

        }

    }
}
