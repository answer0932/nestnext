import { Controller, Get, Render } from '@nestjs/common';
import { ResultsService } from '../../service/results/results.service';
import { UtilsService } from '../../service/utils/utils.service';

@Controller('results')
export class ResultsController {

    constructor(private readonly resultsService: ResultsService, private readonly utilsService: UtilsService) { }

    @Get()
    @Render('Results/IndexPage')
    async index() {

        try {
            const data = await this.resultsService.resultaData();

            return this.utilsService.publicDataFormat(data.data, 'ok', true);

        } catch (error) {

            return this.utilsService.publicDataFormat(null, 'no', false);

        }
    }
}
