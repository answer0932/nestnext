// gulpfile.js
var gulp = require('gulp');
var gulpCopy = require('gulp-copy');

var sourceFiles = ['ssl/*', '.env'];
var outputPath = './.next';

return gulp.task('default', function () {
    return gulp.src(sourceFiles)
        .pipe(gulpCopy(outputPath, {}))
})

