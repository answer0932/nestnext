export type VideoTypes = {
    mp4: {
        src: string;
        type: string;
        poster: string;
    }
}

export type SwiperTypes = Array<{
    id: number;
    url: string;
}>

export type LotteryTypes = {
    price: number;
    translate: string;
    week: string;
    date: string;
    time: string;
    slot: 'AM' | 'PM';
    ball: Array<number>;
    odd: string;
    countDown: number;
}
