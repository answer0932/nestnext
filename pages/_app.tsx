import { FC } from 'react';
import { RecoilRoot } from 'recoil';
import type { AppProps /*, AppContext */ } from 'next/app';
import Header from '@components/header';
import Footer from '@components/footer';
import SSRProvider from 'react-bootstrap/SSRProvider';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'styles/globals.css';

const MyApp: FC<AppProps> = ({ Component, pageProps }) => {
	return (
		<RecoilRoot>
			<SSRProvider>
				<div className="bg-light">
					<Header />
					<Component {...pageProps} />
					<Footer />
				</div>
			</SSRProvider>
		</RecoilRoot>
	);
};

export default MyApp;