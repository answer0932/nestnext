export type DateSelectionTypes = Array<{
    data: string;
    id: number;
}>

export type DateListTypes = {
    page: 1,
    total: 20,
    amount: 10,
    data: Array<{
        date: string;
        number: [number];
        details: Array<{
            division: string;
            total: string;
            winner: string;
        }>
    }>
}

