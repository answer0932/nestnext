import React, { FC, useMemo, useEffect } from "react";
import videojs from "video.js";
import "video.js/dist/video-js.css";


export const VideoJS: FC<any> = (props: any) => {

    const videoRef = React.useRef(null);
    const playerRef: any = React.useRef(null);
    const { options, onReady } = props;

    useEffect(() => {
        // make sure Video.js player is only initialized once
        if (!playerRef.current) {

            const videoElement = videoRef.current;

            if (!videoElement) return;

            const player: any = playerRef.current = videojs(videoElement, options, () => {
                console.log("player is ready");
                onReady && onReady(player);
            });
        } else {
            // you can update player here [update player through props]
            const player = playerRef.current;
            player.autoplay(options.autoplay);
            player.src(options.sources);
        }

    }, [options, onReady]);

    // Dispose the Video.js player when the functional component unmounts
    useEffect(() => {
        return () => {
            if (playerRef.current) {
                playerRef.current.dispose();
                playerRef.current = null;
            }
        };
    }, []);

    return (
        <div data-vjs-player>
            <video ref={videoRef} className="video-js vjs-big-play-centered" />
        </div>
    );
};

const VideoView = () => {

    const playerRef = React.useRef(null);

    return useMemo(() => {

        const videoJsOptions = { // lookup the options in the docs for more options
            autoplay: false,
            controls: true,
            responsive: true,
            preload: 'auto',
            fluid: true,
            language: 'zh-CN', // 设置语言
            inactivityTimeout: 0,
            muted: false, // 是否静音
            controlBar: { // 设置控制条组件
                /* 设置控制条里面组件的相关属性及显示与否
                'currentTimeDisplay':true,
                'timeDivider':true,
                'durationDisplay':true,
                'remainingTimeDisplay':false,
                volumePanel: {
                  inline: false,
                }
                */
                /* 使用children的形式可以控制每一个控件的位置，以及显示与否 */
                children: [
                    { name: 'playToggle' }, // 播放按钮
                    { name: 'currentTimeDisplay' }, // 当前已播放时间
                    { name: 'progressControl' }, // 播放进度条
                    { name: 'durationDisplay' }, // 总时间
                    { // 倍数播放
                        name: 'playbackRateMenuButton',
                        children: [0.5, 1, 1.5, 2, 2.5]
                    },
                    {
                        name: 'volumePanel', // 音量控制
                        inline: false, // 不使用水平方式
                    },
                    { name: 'FullscreenToggle' } // 全屏
                ]
            },
            sources: [
                {
                    src: '//vjs.zencdn.net/v/oceans.mp4',
                    type: 'video/mp4',
                    poster: '//vjs.zencdn.net/v/oceans.png'
                }
            ]
        }

        const handlePlayerReady = (player: any) => {
            playerRef.current = player;

            // you can handle player events here
            player.on('waiting', () => {
                console.log('player is waiting');
            });

            player.on('dispose', () => {
                console.log('player will dispose');
            });
        };

        return (
            <div
                className="border-white"
                style={{ borderWidth: 20 + 'px', borderStyle: 'solid', position: 'relative', zIndex: 1 }}>
                <VideoJS
                    options={videoJsOptions}
                    onReady={(e: any) => handlePlayerReady(e)} />
            </div>
        );
    }, [playerRef]);
}

export default VideoView;