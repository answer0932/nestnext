import React, { useState, useCallback, useRef } from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';
import Moment from 'moment';

type PropsType = {
    data: {
        ball: Array<string | number>;
        odd: string;
    }
}

const Right = () => {

    // const [ball, setBall] = useState<Array<string | number>>(["？", "？", "？"]);
    // const [odd, setOdd] = useState(Moment().format("Odd of Winning: ? in ?"));
    Moment()
    return (
        <Col xs={12} lg={7} className="ps-5 pt-4">
            <h3>Pick 3 With Rome Results</h3>
            <p>Select a draw period</p>

            <Row>
                <Col lg={7} >
                    <Form.Select>
                        <option>Default select</option>
                    </Form.Select>
                    <br />
                </Col>
            </Row>

            <h6>Warning Numbers</h6>
            <p className="ball-wrapper">
                {/* {
                    ball.map((ball, index) => <span className="ball fs-3 me-3" key={index}>{ball}</span>)
                } */}

            </p>
            <div className="d-flex justify-content-between align-items-center pt-4">
                {/* <h6>{odd}</h6> */}
                <Button className="rounded-pill">CHECK MY NUMBERS</Button>
            </div>
        </Col>
    )
}

export default Right;