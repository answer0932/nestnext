import { Module } from '@nestjs/common';
import { RenderModule } from 'nest-next';
import Next from 'next';
import { ContactController } from './controller/contact/contact.controller';
import { HomeController } from './controller/home/home.controller';
import { AboutController } from './controller/about/about.controller';
import { RulesController } from './controller/rules/rules.controller';
import { FaqsController } from './controller/faqs/faqs.controller';
import { ResultsController } from './controller/results/results.controller';
import { HomeService } from './service/home/home.service';
import { AboutService } from './service/about/about.service';
import { FaqsService } from './service/faqs/faqs.service';
import { RulesService } from './service/rules/rules.service';
import { ResultsService } from './service/results/results.service';
import { ContactService } from './service/contact/contact.service';
import { UtilsService } from './service/utils/utils.service';
import { FetchController } from './controller/fetch/fetch.controller';

@Module({
	imports: [
		RenderModule.forRootAsync(
			Next({
				dev: process.env.NODE_ENV !== 'production',
				conf: { useFilesystemPublicRoutes: false },
			}),
		),
	],
	controllers: [ContactController, HomeController, AboutController, RulesController, FaqsController, ResultsController, FetchController],
	providers: [HomeService, AboutService, FaqsService, RulesService, ResultsService, ContactService, UtilsService],
})
export class AppModule { }