import React from 'react';
import { NextPage, NextPageContext } from 'next';
import { Container } from 'react-bootstrap';
import Jumbotron from 'components/jumbotron';
import { LotteryTypes, VideoTypes } from '@interfaces/home';
import { RulesService } from '@/service/rules/rules.service';

type PropsType = {
    data: {
        lottery: LotteryTypes;
        isServer: boolean;
        video: VideoTypes;
    }
};

const RulesView: NextPage<PropsType> = ({ data }: PropsType) => {

    return (
        <section className="my-5">
            <Jumbotron data={{ lottery: data.lottery, video: data.video }} />
            <Container className="my-5 py-5 bg-white px-5 py5">
                <h4 className="pb-3 text-danger">How to Play Pick 3 With Rome</h4>
                <p className="pb-3">
                    Pick 3 offers players a day drawing and a night drawing, which means two chances to win each day. You could win up to $500 playing the base game! Plus, the FIREBALL add-on available in retail stores gives players the chance to create additional number combinations for another chance to win! Click for a closer view of the playslip to learn more.<br />
                    Here how to play Pick 3:<br />
                    Choose your play amount. You can play Pick 3 for $1 or 50¢.<br />
                    Choose your play type. You ve got options, so see the Play types section for detailed descriptions.<br />
                    Choose your draw. You can play a day draw, night draw or both!<br />
                    Choose your own numbers on a playslip. Pick a three-digit number from 000 through 999.<br />
                    Or use Easy Pick, and let the computer choose your numbers for you.<br />
                    The day drawing takes place daily at 1:59 p.m., and the night drawing takes place daily at 11 p.m.<br />
                    The Pick 3 cutoff time occurs daily at 1:53 p.m. ET for the day drawing and at 10:45 p.m. ET for the night drawing.
                </p>
                <h4 className="pb-3 text-danger">Play types</h4>
                <p className="pb-3">
                    One of the things that makes Pick 3 fun is that there are so many ways to play! Here they are.<br />

                    Exact Order: Play it straight! Match all three digits in the same order as drawn.<br />
                    Any Order: Give yourself options! Match all three of the digits drawn; the order doesn t matter.<br />
                    50/50: The best of both worlds — split a $1 play in half! Put 50¢ on Exact Order and 50¢ on Any Order.<br />
                    Combo: Cover all the bases! Play Exact Order on every possible combination of a three-digit number.<br />
                    Pairs: Chose only two digits! Win with an Exact Order match regardless of what third digit is drawn.<br />
                </p>
                <h4 className="pb-3 text-danger">How to Play Pick 3 With Rome</h4>
                <p className="pb-3">Pick 3 offers players a day drawing and a night drawing, which means two chances to win each day. You could win up to $500 playing the base game! Plus, the FIREBALL add-on available in retail stores gives players the chance to create additional number combinations for another chance to win! Click for a closer view of the playslip to learn more.<br />

                    Heres how to play Pick 3:<br />

                    Choose your play amount. You can play Pick 3 for $1 or 50¢.<br />
                    Choose your play type. You ve got options, so see the Play types section for detailed descriptions.<br />
                    Choose your draw. You can play a day draw, night draw or both!<br />
                    Choose your own numbers on a playslip. Pick a three-digit number from 000 through 999.<br />
                    Or use Easy Pick, and let the computer choose your numbers for you.<br />
                    The day drawing takes place daily at 1:59 p.m., and the night drawing takes place daily at 11 p.m.<br />
                    The Pick 3 cutoff time occurs daily at 1:53 p.m. ET for the day drawing and at 10:45 p.m. ET for the night drawing.</p>
            </Container>
        </section>
    )
}

export const getServerSideProps = async (ctx: NextPageContext) => {

    const isServer = Object.keys(ctx.query);

    if (isServer.length > 0) {

        return { props: Object.assign({ ...ctx.query }, { isServer: true }) };

    } else {

        const data: any = await new RulesService().rulesData();

        return { props: Object.assign({ ...data }, { isServer: false }) };
    }
}


export default RulesView;