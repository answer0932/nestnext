import React from 'react';
import PropTypes from 'prop-types';
import { Container } from 'react-bootstrap';

type propType = {
    title: string,
    content: string
}

const IntroduceView = ({ title, content }: propType) => {
    return (
        <div className="introduce__banner py-5 text-white">
            <Container>
                <h2>{title}</h2>
                <h6>{content}</h6>
            </Container>
        </div>
    )
}

IntroduceView.propTypes = {
    title: PropTypes.string,
    content: PropTypes.string
}

export default IntroduceView;