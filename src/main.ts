import config from './config';
import * as fs from 'fs';
import { join } from 'path';

import { NestExpressApplication, ExpressAdapter } from '@nestjs/platform-express';
import * as spdy from 'spdy'
import { NestFactory } from '@nestjs/core';
import express from 'express';
import { createServer } from 'http';

import { AppModule } from './application.module';

const httpsOptions = {
	key: fs.readFileSync(join(__dirname, '../', 'ssl/key.pem')),
	cert: fs.readFileSync(join(__dirname, '../', 'ssl/cert.pem'))
};

async function bootstrap() {

	const expressApp = express();

	const app = await NestFactory.create<NestExpressApplication>(AppModule, new ExpressAdapter(expressApp));

	const server = spdy.createServer(httpsOptions, expressApp);

	await app.init();

	createServer(expressApp).listen(config.httpPort, () => {
		console.log(`server is running at http://localhost:${config.httpPort}`);
	});

	await server.listen(config.httpsPort, () => {
		console.log(`server is running at https://localhost:${config.httpsPort}`);
	});
}

bootstrap();