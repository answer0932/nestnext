import React from 'react';
import { Container, Row } from 'react-bootstrap';
import ProtoType from 'prop-types';
import dynamic from 'next/dynamic';

import { LotteryTypes } from 'interfaces/home.d';

const Left = dynamic(() => import(
    /* webpackChunkName: "LotteryResultLeft" */
    /* webpackMode: "lazy" */
    "./LotteryResultModules/Left"), { ssr: true, });

const Right = dynamic(() => import(
    /* webpackChunkName: "LotteryResultRight" */
    /* webpackMode: "lazy" */
    "./LotteryResultModules/Right"), { ssr: true, });

type LotteryResultDataTypes = {
    data: LotteryTypes
}

const LotteryResultView = ({ data }: LotteryResultDataTypes) => {

    return (
        <div className="bg-white mt-3">
            <Container>
                <Row>
                    <Left data={data} />
                    <Right />
                </Row>
            </Container>
        </div >
    )
}

LotteryResultView.prototype = {
    data: ProtoType.objectOf(ProtoType.shape({
        ball: ProtoType.arrayOf(ProtoType.number),
        countDown: ProtoType.number,
        date: ProtoType.string,
        odd: ProtoType.string,
        price: ProtoType.number,
        slot: ProtoType.string,
        time: ProtoType.string,
        translate: ProtoType.string,
        week: ProtoType.string
    }))
};

export default LotteryResultView;