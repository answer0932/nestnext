import { Controller, Get, Render } from '@nestjs/common';

@Controller('rules')
export class RulesController {
    @Get()
    @Render("Rules/IndexPage")
    index() {

    }
}
