import { FC, useEffect } from 'react';
import Link from 'next/link';
import { useState, useMemo } from 'react';
import { Navbar, Container, Nav, NavDropdown, Button } from 'react-bootstrap';
import { currentPath } from 'utils/routerHandle.util';

type NavigationType = Array<{
    name: string;
    href: string;
    prefetch: boolean;
    as: string;
    key: number;
}>

const navigation: NavigationType = [
    { name: 'Home', href: '/views/Home/IndexPage', prefetch: false, as: "/", key: 0 },
    { name: 'Results', href: '/views/Results/IndexPage', prefetch: false, as: "/results", key: 1 },
    { name: 'Rules', href: '/views/Rules/IndexPage', prefetch: false, as: "/rules", key: 2 },
    { name: 'FAQs', href: '/views/FAQs/IndexPage', prefetch: false, as: "/faqs", key: 3 },
    { name: 'Contact', href: '/views/Contact/IndexPage', prefetch: false, as: "/contact", key: 4 },
    { name: 'About', href: '/views/About/IndexPage', prefetch: false, as: "/about", key: 5 },
]

const HeaderView: FC = () => {

    const [currentId, setCurrentId] = useState(0);
    const [navBarStatus, setNavBarStatus] = useState(false);

    const path = navigation.filter(ele => ele.as === currentPath());

    useEffect(() => {

        if (path.length > 0) {
            setCurrentId(path[0].key);
        }

    }, [path])


    return useMemo(() => {

        return (
            <Navbar bg="white py-4" expand="lg" onToggle={(status: boolean) => setNavBarStatus(status)}>
                <Container className="d-flex">
                    <Navbar.Brand href="#home">LOGO</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav" className="flex-grow-0">
                        <Nav>
                            {navigation.map((item) => (
                                <Link {...item} key={item.name}>
                                    {<Button
                                        onClick={() => setCurrentId(item.key)}
                                        variant={item.key === currentId ? 'primary' : 'light'}
                                        disabled={item.key === currentId ? true : false}
                                        className={`${!navBarStatus ? 'rounded-pill' : ''} me-3`}
                                    >
                                        {item.name}
                                    </Button>}
                                </Link>
                            ))}
                            <NavDropdown title="Language" id="basic-nav-dropdown" className="dropdown-menu-lg-end d-flex align-items-center">
                                <NavDropdown.Item href="#action/3.1">ENGLISH</NavDropdown.Item>
                                <NavDropdown.Item href="#action/3.2">Chinese Simplified</NavDropdown.Item>
                                <NavDropdown.Item href="#action/3.3">Chinese Traditional</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        );
    }, [currentId, navBarStatus]);
};

export default HeaderView;