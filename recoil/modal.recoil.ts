import { atom, selector } from 'recoil';


const modalState = atom<any>({
    key: 'modalState',
    default: false,
});


const setModalState: any = selector<any>({
    key: "setModalState",
    get: ({ get }: any) => {
        const status: any = get("modalState");
        return status;
    }
})

export { modalState, setModalState }