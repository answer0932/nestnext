import { Controller, Get, Render } from '@nestjs/common';

@Controller('contact')
export class ContactController {
    @Get()
    @Render("Contact/IndexPage")
    index() {

    }
}
