import { join } from 'path';

type Config = {
    viewDir?: string,
    assetsDir?: string,
    httpPort?: number,
    httpsPort?: number,
    noCache?: boolean,
}

// 公共配置
let config: Config = {
    viewDir: join(__dirname, '../..', `assets/views`),
    assetsDir: join(__dirname, '../..', 'assets'),
}


// 开发
if (process.env.NODE_ENV === 'development') {
    const devConfig: Config = {
        httpPort: 3000,
        httpsPort: 3030,
    }

    config = {
        ...config,
        ...devConfig,
    }
}

// 生产
if (process.env.NODE_ENV === 'production') {

    const prodConfig: Config = {
        httpPort: 90,
        httpsPort: 9090,
    }

    config = {
        ...config,
        ...prodConfig,
    }
}

export default config;