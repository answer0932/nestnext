import React from 'react';
import dynamic from 'next/dynamic';

import { NextPage, NextPageContext } from 'next';
import { LotteryTypes } from '@interfaces/home';
import { Container } from 'react-bootstrap';

import { HomeService } from '@/service/home/home.service';

type PropsType = {
    data: {
        lottery: LotteryTypes;
        isServer: boolean;
    }
};

const Swiper = dynamic(() => import(
    /* webpackChunkName: "swiper" */
    /* webpackMode: "lazy" */
    "components/swiper"), { ssr: false, });

const Video = dynamic(() => import(
    /* webpackChunkName: "video" */
    /* webpackMode: "lazy" */
    "components/video"), { ssr: false, });

const LotteryResult = dynamic(() => import(
    /* webpackChunkName: "lotteryResult" */
    /* webpackMode: "lazy" */
    "./modules/LotteryResult"), { ssr: true, });

const CheckMyNumbers = dynamic(() => import(
    /* webpackChunkName: "checkNumbers" */
    /* webpackMode: "lazy" */
    "./modules/CheckMyNumbers"), { ssr: true, });

const GameRules = dynamic(() => import(
    /* webpackChunkName: "gameRules" */
    /* webpackMode: "lazy" */
    "./modules/GameRules"), { ssr: true, });




const Home: NextPage<PropsType> = ({ data }: PropsType) => {

    console.log('home-------父组件更新');

    return (
        <div>
            <Swiper />
            <Container style={{ marginTop: `${-100}px` }}>
                <Video />
                <LotteryResult data={data.lottery} />
                <CheckMyNumbers />
                <GameRules />
            </Container>
        </div>
    )
};

export const getServerSideProps = async (ctx: NextPageContext) => {

    const isServer = Object.keys(ctx.query);

    if (isServer.length > 0) {

        return { props: Object.assign({ ...ctx.query }, { isServer: true }) };

    } else {

        const data: any = await new HomeService().homeData();

        return { props: Object.assign({ ...data }, { isServer: false }) };
    }

};

export default Home;