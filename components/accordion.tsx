import React from 'react';
import { Container, Accordion, Row, Col } from 'react-bootstrap';

const AccordionView = () => {
    return (
        <Accordion defaultActiveKey="0" flush className="mt-4">
            {
                Array(10).fill(1).map((i, k) => <Accordion.Item eventKey={String(k)} key={k}>
                    <Accordion.Header>
                        <Container>
                            <Row>
                                <Col className="d-flex justify-content-start">313-10-04-2021</Col>
                                <Col>
                                    <p>Winning Numbers:</p>
                                    {/* <Row>
                                        <Col className="d-flex justify-content-start"><span className="ball--small">3</span></Col>
                                        <Col className="d-flex justify-content-start"><span className="ball--small">3</span></Col>
                                        <Col className="d-flex justify-content-start"> <span className="ball--small">4</span></Col>
                                    </Row> */}
                                    <div className="d-flex justify-content-start"><span className="ball--small">3</span><span className="ball--small">3</span><span className="ball--small">3</span></div>
                                </Col>
                            </Row>
                        </Container>
                    </Accordion.Header>
                    <Accordion.Body>
                        <Container>
                            <Row>
                                <Col className="d-flex justify-content-center">Division</Col>
                                <Col className="d-flex justify-content-center">Total interest*</Col>
                                <Col className="d-flex justify-content-center">Winners**</Col>
                            </Row>
                            <Row>
                                <Col className="d-flex justify-content-center">Division 1</Col>
                                <Col className="d-flex justify-content-center">Division 2</Col>
                                <Col className="d-flex justify-content-center">Division 3</Col>
                            </Row>
                            <Row>
                                <Col className="d-flex justify-content-center">€ 56,800.15</Col>
                                <Col className="d-flex justify-content-center">€698.86</Col>
                                <Col className="d-flex justify-content-center">€47.51</Col>
                            </Row>
                        </Container>
                    </Accordion.Body>
                </Accordion.Item>)
            }
        </Accordion>
    )
}

export default AccordionView;