import { useKeenSlider } from "keen-slider/react";
import Image from 'next/image';
import "keen-slider/keen-slider.min.css";
import _ from 'lodash';

const SwiperView = () => {

    const [ref] = useKeenSlider<HTMLDivElement>();

    const images = [
        '/images/banner/0.jpg',
        '/images/banner/1.jpg',
        '/images/banner/2.jpg'
    ];



    return (
        <div ref={ref} className="keen-slider" style={{ height: 528 + 'px' }}>
            {_.map(images, (image) => <div
                className="keen-slider__slide number-slide1"
                key={image}>
                <Image
                    src={image}
                    layout="fill"
                    alt={image}
                    loading={'lazy'}
                />
            </div>)})
        </div>
    )
}

export default SwiperView;