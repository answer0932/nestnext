import { Injectable } from '@nestjs/common';

@Injectable()
export class UtilsService {
    publicDataFormat(data: any, msg: string, status: boolean) {
        return {
            data,
            msg,
            status
        }
    }
}
