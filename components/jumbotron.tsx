import React, { useState, useRef, useCallback, useEffect } from 'react';
import { Container, Alert, Row, Col, Button } from 'react-bootstrap';
import { useEffectOnce } from 'react-use';
import ProtoType from 'prop-types';
import { LotteryTypes, VideoTypes } from 'interfaces/home.d';
import CountDown from 'components/countDown';
import ModalView from 'components/modal';
import { useRecoilCallback } from 'recoil';
import { modalState } from 'recoil/modal.recoil';

type PropsType = {
    data: {
        lottery: LotteryTypes,
        video: VideoTypes
    }
}

const JumbotronView = ({ data }: PropsType) => {

    const [show, setShow] = useState(true);
    const [status, setStatus] = useState(true);
    const [count, setCount] = useState(10);

    /**
     * @func setRecoilModelState 
     * @desc recoil manage state modal show or hide is status
     */
    const setRecoilModelState = useRecoilCallback(({ set }) => async () => {
        set(modalState, true);
    });

    const timer: any = useRef();

    /**
     * @func countDownHandle
     * @desc if count = 0 just use fetch get new Data , get new data after must use countDown go to setCount; 
     */
    const countDownHandle = async () => {
        const response = await fetch("/lottery");
        const { data } = await response.json();

        if (data) {
            const { countDown } = data;
            setCount(countDown);
        }
    };

    /**
     * @func installDataHandle 
     * @desc only once use this function handle
     */
    const installDataHandle = useCallback(() => {
        if (!status) {
            setCount(10000);
        } else {
            const { countDown } = data.lottery;
            setCount(countDown);
        }
    }, [status, data]);

    /**
     * @func startHandle
     * @desc setTimeout countdown function, get new date of countDown field put setCount
     *       if > 0 Diminishing else use countDownHandle get new data and Destroy once setTimeout
     */
    const startHandle = useCallback(() => {
        timer.current = setInterval(() => {
            if (count > 0) {
                setCount(count - 1);
            } else {
                countDownHandle();
                clearInterval(timer.current);
            }
        }, 1000);
    }, [count]);

    useEffectOnce(() => {
        console.log('render once Effect to once');

        if (data) {
            setStatus(true);
        } else {
            setStatus(false);
        }

        installDataHandle();
    });

    useEffect(() => {

        console.log('start');

        startHandle();

        if (!show) {
            clearInterval(timer.current);
        }

        return () => {
            clearInterval(timer.current);
        };

    }, [show, count, startHandle]);

    return (
        <>
            {show ? <Container className="bg-danger">
                <Alert onClose={() => setShow(false)} dismissible className="bg-danger border-0">
                    <Row className="justify-content-lg-between">
                        <Col lg={4} className="row align-items-center">
                            <span className="text-white my-0">Jackpot</span>
                            <h1 className="text-white my-0">{data.lottery.translate} {data.lottery.price.toFixed(2)}</h1>
                            <p className="text-white my-0">{data.lottery.odd}</p>
                        </Col>
                        <Col lg={3}>
                            <CountDown count={count} />
                        </Col>
                        <Col lg={2} className="d-flex">
                            <Row className="justify-content-center align-items-center">
                                <Col lg={12}>
                                    <Button
                                        variant="warning"
                                        className="text-white"
                                        onClick={setRecoilModelState}
                                    >LIVE DRAW</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Alert>
            </Container> : ""}
            <ModalView title={'11111111111111'} />
        </>
    )
}

JumbotronView.prototype = {
    data: ProtoType.objectOf(ProtoType.shape({
        lottery: ProtoType.objectOf(ProtoType.shape({
            ball: ProtoType.arrayOf(ProtoType.number),
            countDown: ProtoType.number,
            date: ProtoType.string,
            odd: ProtoType.string,
            price: ProtoType.number,
            slot: ProtoType.string,
            time: ProtoType.string,
            translate: ProtoType.string,
            week: ProtoType.string
        })),
        video: ProtoType.objectOf(ProtoType.shape({
            mp4: ProtoType.objectOf(ProtoType.shape({
                src: ProtoType.string,
                type: ProtoType.string,
                poster: ProtoType.string
            }))
        }))
    }))
}

export default JumbotronView;