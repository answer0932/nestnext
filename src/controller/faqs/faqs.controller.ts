import { Controller, Get, Render } from '@nestjs/common';

@Controller('faqs')
export class FaqsController {
    @Get()
    @Render('FAQs/IndexPage')
    index() {

    }
}
