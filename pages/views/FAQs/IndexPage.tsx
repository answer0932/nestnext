import React from 'react';
import { Container, Accordion } from 'react-bootstrap';
import Introduce from 'components/introduce';

const FAQs = () => {
    return (
        <section className="mb-5">
            <Introduce title="FAQs" content="Get answers to all your lottery related frequently asked questions." />
            <Container className="mt-5">
                <Accordion defaultActiveKey="0">
                    <Accordion.Item eventKey="0">
                        <Accordion.Header>
                            <span className="text-danger">Are the rules different if I play the lottery online?</span>
                        </Accordion.Header>
                        <Accordion.Body>
                            No, the rules are the same. When you play online, you play the same game so the rules and prizes available are the same as if you had played at a store.
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="1">
                        <Accordion.Header>
                            <span className="text-danger">Are the rules different if I play the lottery online?</span>
                        </Accordion.Header>
                        <Accordion.Body>
                            No, the rules are the same. When you play online, you play the same game so the rules and prizes available are the same as if you had played at a store.
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="2">
                        <Accordion.Header>
                            <span className="text-danger">Are the rules different if I play the lottery online?</span>
                        </Accordion.Header>
                        <Accordion.Body>
                            No, the rules are the same. When you play online, you play the same game so the rules and prizes available are the same as if you had played at a store.
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="3">
                        <Accordion.Header>
                            <span className="text-danger">Are the rules different if I play the lottery online?</span>
                        </Accordion.Header>
                        <Accordion.Body>
                            No, the rules are the same. When you play online, you play the same game so the rules and prizes available are the same as if you had played at a store.
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="4">
                        <Accordion.Header>
                            <span className="text-danger">Are the rules different if I play the lottery online?</span>
                        </Accordion.Header>
                        <Accordion.Body>
                            No, the rules are the same. When you play online, you play the same game so the rules and prizes available are the same as if you had played at a store.
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="5">
                        <Accordion.Header>
                            <span className="text-danger">Are the rules different if I play the lottery online?</span>
                        </Accordion.Header>
                        <Accordion.Body>
                            No, the rules are the same. When you play online, you play the same game so the rules and prizes available are the same as if you had played at a store.
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="6">
                        <Accordion.Header>
                            <span className="text-danger">Are the rules different if I play the lottery online?</span>
                        </Accordion.Header>
                        <Accordion.Body>
                            No, the rules are the same. When you play online, you play the same game so the rules and prizes available are the same as if you had played at a store.
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="7">
                        <Accordion.Header>
                            <span className="text-danger">Are the rules different if I play the lottery online?</span>
                        </Accordion.Header>
                        <Accordion.Body>
                            No, the rules are the same. When you play online, you play the same game so the rules and prizes available are the same as if you had played at a store.
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="8">
                        <Accordion.Header>
                            <span className="text-danger">Are the rules different if I play the lottery online?</span>
                        </Accordion.Header>
                        <Accordion.Body>
                            No, the rules are the same. When you play online, you play the same game so the rules and prizes available are the same as if you had played at a store.
                        </Accordion.Body>
                    </Accordion.Item>
                </Accordion>
            </Container>
        </section>
    )
}

export default FAQs;