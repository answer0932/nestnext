import React, { useState } from 'react';
import { NextPage, NextPageContext } from 'next';
import { Container, Form, Button, Pagination, Row, Col } from 'react-bootstrap';
import Jumbotron from 'components/jumbotron';
import Accordion from 'components/accordion';
import { LotteryTypes, VideoTypes } from '@interfaces/home';
import { DateSelectionTypes, DateListTypes } from 'interfaces/results.d';
import { ResultsService } from '@/service/results/results.service';

type PropsType = {
    data: {
        list: DateListTypes;
        selection: DateSelectionTypes;
        lottery: LotteryTypes;
        isServer: boolean;
        video: VideoTypes
    }
};

const ResultsView: NextPage<PropsType> = ({ data }: PropsType) => {

    console.log('result-------父组件更新');

    const [active, setActive] = useState(1);

    const items = [];

    for (let number = 1; number <= 5; number++) {
        items.push(
            <Pagination.Item key={number} active={number === active} onClick={() => setActive(number)}>
                {number}
            </Pagination.Item>,
        );
    }

    return (
        <section className="my-5">
            <Jumbotron data={{ lottery: data.lottery, video: data.video }} />

            <Container className="bg-white py-4">
                <h1>Past Numbers</h1>
                <Row className="d-flex justify-content-between">
                    <Col lg={3}>
                        <Form.Group controlId="formGridState">
                            <Form.Label>Selection Date</Form.Label>
                            <Form.Select defaultValue="Choose...">
                                <option>Choose...</option>
                                <option>...</option>
                            </Form.Select>
                        </Form.Group>
                    </Col>
                    <Col lg={3}>
                        <Form.Label>Search a draw period</Form.Label>
                        <Form.Group controlId="formGridState" className="d-flex">
                            <Form.Control type="search" placeholder="Enter Search" aria-label="Search" className="form-control me-2" />
                            <Button variant="success" type="submit">
                                Submit
                            </Button>
                        </Form.Group>
                    </Col>
                </Row>

                <Accordion />

                <Pagination className="justify-content-center mt-5">
                    <Pagination.First onClick={() => setActive(1)} />
                    <Pagination.Prev onClick={() => setActive(active - 1)} />
                    <Pagination>{items}</Pagination>
                    <Pagination.Next onClick={() => setActive(active + 1)} />
                    <Pagination.Last onClick={() => setActive(5)} />
                </Pagination>
            </Container>
        </section>
    )
}

export const getServerSideProps = async (ctx: NextPageContext) => {

    const isServer = Object.keys(ctx.query);

    if (isServer.length > 0) {

        return { props: Object.assign({ ...ctx.query }, { isServer: true }) };

    } else {

        const data: any = await new ResultsService().resultaData();

        return { props: Object.assign({ ...data }, { isServer: false }) };
    }
}

export default ResultsView;