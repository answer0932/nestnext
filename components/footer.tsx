import React, { FC } from 'react';
import Link from 'next/link';
import { Container, Row, Col } from 'react-bootstrap'

type NavigationType = Array<{
    name: string;
    href: string;
    prefetch: boolean;
    as: string;
    key: number;
}>

const navigation: NavigationType = [
    { name: 'Home', href: '/views/Home/IndexPage', prefetch: false, as: "/", key: 0 },
    { name: 'Results', href: '/views/Results/IndexPage', prefetch: false, as: "/results", key: 1 },
    { name: 'Rules', href: '/views/Rules/IndexPage', prefetch: false, as: "/rules", key: 2 },
    { name: 'FAQs', href: '/views/FAQs/IndexPage', prefetch: false, as: "/faqs", key: 3 },
    { name: 'Contact', href: '/views/Contact/IndexPage', prefetch: false, as: "/contact", key: 4 },
    { name: 'About', href: '/views/About/IndexPage', prefetch: false, as: "/about", key: 5 },
]

const FooterView: FC = () => {

    return (
        // Copyright 2021 Virginia Lottery All Rights Reserved
        <footer className="bg-black py-5 text-white mt-5">
            <Container className="d-flex container-fluid">
                <div className="container-sm py-5 text-light">
                    <Row>
                        <Col lg={2}>
                            LOGO
                        </Col>
                        <Col md={"auto"} lg={9} className="d-flex flex-column justify-content-center">
                            <p>
                                {navigation.map((item, index) => (
                                    <span className={`${index !== navigation.length - 1 ? 'border-end' : ''} ${index === 0 ? 'pe-3' : 'px-3'} link-light`} key={index}>
                                        <Link {...item} key={item.name}>
                                            {item.name}
                                        </Link>
                                    </span>
                                ))}
                                {/* <a className="border-end pe-3 link-light" href="/about">About Us</a>
                                <a className="border-end px-3 link-light" href="/rules">Game Rules</a>
                                <a className="border-end px-3 link-light" href="/faqs">FAQs</a>
                                <a className="border-end px-3 link-light" href="/contact">Contact</a>
                                <a className="px-2 link-light" href="/results">Terms & Conditions</a> */}
                            </p>
                            <p>Copyright 2021 Virginia Lottery All Rights Reserved</p>
                        </Col>
                        <Col lg={1} className="col col-lg-1">
                            <p>Follow Us</p>
                            <span className="pe-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="8.037" height="15.903" viewBox="0 0 8.037 15.903">
                                    <path id="facebook"
                                        d="M50.252,24.173H52v7.178a.254.254,0,0,0,.252.252h2.958a.254.254,0,0,0,.252-.252V24.208h2.006a.259.259,0,0,0,.252-.229l.31-2.649a.21.21,0,0,0-.069-.195.236.236,0,0,0-.195-.08H55.469V19.392c0-.5.264-.757.8-.757h1.513a.254.254,0,0,0,.252-.252V15.952a.254.254,0,0,0-.252-.252H55.607a3.981,3.981,0,0,0-2.614.986,2.7,2.7,0,0,0-.906,2.431v1.938H50.252a.254.254,0,0,0-.252.252v2.614A.254.254,0,0,0,50.252,24.173Z"
                                        transform="translate(-50 -15.7)" fill="#fff" />
                                </svg>
                            </span>

                            <span className="pe-1">
                                <svg id="twitter" xmlns="http://www.w3.org/2000/svg" width="14.813" height="12.039"
                                    viewBox="0 0 14.813 12.039">
                                    <g id="组_67" data-name="组 67" transform="translate(0 0)">
                                        <path id="路径_128" data-name="路径 128"
                                            d="M35.314,33.833a6.442,6.442,0,0,1-1.743.482,3.066,3.066,0,0,0,1.341-1.685,6.169,6.169,0,0,1-1.938.734,3.042,3.042,0,0,0-5.263,2.075,2.693,2.693,0,0,0,.08.688,8.545,8.545,0,0,1-6.272-3.153,3.007,3.007,0,0,0-.413,1.525,3.056,3.056,0,0,0,1.353,2.534,2.977,2.977,0,0,1-1.376-.378v.034a3.038,3.038,0,0,0,2.442,2.981,3.08,3.08,0,0,1-.8.1,3.134,3.134,0,0,1-.573-.057,3.052,3.052,0,0,0,2.843,2.11,6.1,6.1,0,0,1-3.772,1.3,4.987,4.987,0,0,1-.722-.046,8.658,8.658,0,0,0,13.323-7.292l-.011-.39A5.9,5.9,0,0,0,35.314,33.833Z"
                                            transform="translate(-20.5 -32.4)" fill="#fff" />
                                    </g>
                                </svg>
                            </span>

                            <span className="pe-2">
                                <svg id="instagram" xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                                    <path id="路径_125" data-name="路径 125"
                                        d="M34.136,24H27.864A3.866,3.866,0,0,0,24,27.864v6.272A3.866,3.866,0,0,0,27.864,38h6.272A3.866,3.866,0,0,0,38,34.136V27.864A3.88,3.88,0,0,0,34.136,24ZM36.75,34.136a2.613,2.613,0,0,1-2.614,2.614H27.864a2.613,2.613,0,0,1-2.614-2.614V27.864a2.613,2.613,0,0,1,2.614-2.614h6.272a2.613,2.613,0,0,1,2.614,2.614v6.272Z"
                                        transform="translate(-24 -24)" fill="#fff" />
                                    <path id="路径_126" data-name="路径 126"
                                        d="M57.2,53.6a3.6,3.6,0,1,0,3.6,3.6A3.606,3.606,0,0,0,57.2,53.6Zm0,5.962A2.362,2.362,0,1,1,59.562,57.2,2.359,2.359,0,0,1,57.2,59.562Z"
                                        transform="translate(-50.206 -50.206)" fill="#fff" />
                                    <path id="路径_127" data-name="路径 127"
                                        d="M110.806,44.4a.9.9,0,0,0-.906.906.952.952,0,0,0,.264.642.9.9,0,0,0,.642.264.952.952,0,0,0,.642-.264.914.914,0,0,0,0-1.284A.952.952,0,0,0,110.806,44.4Z"
                                        transform="translate(-100.051 -42.061)" fill="#fff" />
                                </svg>
                            </span>

                            <span>
                                <svg id="pinterest" xmlns="http://www.w3.org/2000/svg" width="12.52" height="15.421"
                                    viewBox="0 0 12.52 15.421">
                                    <g id="组_66" data-name="组 66" transform="translate(0 0)">
                                        <path id="路径_124" data-name="路径 124"
                                            d="M36.878,17.7c-4.231,0-6.478,2.706-6.478,5.664a4.132,4.132,0,0,0,2,3.623c.183.08.287.046.332-.126.034-.126.195-.768.275-1.066a.26.26,0,0,0-.069-.264A3.483,3.483,0,0,1,32.2,23.4a4.153,4.153,0,0,1,4.426-4.059,3.79,3.79,0,0,1,4.093,3.807c0,2.534-1.341,4.288-3.084,4.288a1.383,1.383,0,0,1-1.456-1.7,18.313,18.313,0,0,0,.814-3.119A1.2,1.2,0,0,0,35.754,21.3a2.04,2.04,0,0,0-1.777,2.27,3.213,3.213,0,0,0,.3,1.387s-.975,3.921-1.147,4.655A10.48,10.48,0,0,0,33.2,33.03a.1.1,0,0,0,.183.046,12.109,12.109,0,0,0,1.594-3c.115-.436.608-2.224.608-2.224a2.669,2.669,0,0,0,2.247,1.078c2.958,0,5.091-2.6,5.091-5.825C42.92,20.016,40.283,17.7,36.878,17.7Z"
                                            transform="translate(-30.4 -17.7)" fill="#fff" />
                                    </g>
                                </svg>
                            </span>
                        </Col>
                    </Row>
                </div>
            </Container>
        </footer >)
}

export default FooterView;